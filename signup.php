<!DOCTYPE html>
<html lang="en" class="js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="SKYVPN Best VPN for any devices">
        <meta name="keywords" content="SKYVPN, best vpn, vpn service, fastest vpn, anonymus service, anonymus vpn, bypass browser lock, bypass site lock, hide ip, change ip,">
        <title>SKYVPN</title>
        <!--Favicon add-->
        <link rel="shortcut icon" type="image/png" href="/assets/images/logo/icon.png">
        <!--Style Css-->
        <link href="css/second.css" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
        <link href="/assets/front/css/vendor.bundle.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100italic,300,300italic,400,400italic,500,500italic,700,700italic,900,900italic" rel="stylesheet" type="text/css" />
        <!--Font Awasome-->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
        <link rel="stylesheet" href="../assets/front/css/FontAwasomeCSS/font-awesome.css">
        <!--SCRIPTS-->

        <script src="/assets/admin/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
        <script type="text/javascript" src="/assets/user/js/functions.js"></script>
        <script type="text/javascript" src="/assets/front/js/script.js"></script>
    </head>
    <body>
    <header class="site-header">
        <!--navbar area start-->
        <div class="navbar navbar-expand-lg is-transparent" id="mainnav">
			<nav class="container">
				<a class="navbar-brand animated" data-animate="fadeInDown" data-delay=".65" href="/">
					<img alt="logo" class="logo" src="/assets/front/images/logo.png">
				</a>
				<div class="collapse navbar-collapse justify-content-end" id="navbarToggle">
					<ul class="navbar-nav animated remove-animation" data-animate="fadeInDown" data-delay=".75">
						<li class="nav-item">
							<a class="nav-link menu-link" href="features">Features</a>
						</li>
						<li class="nav-item">
							<a class="nav-link menu-link" href="support">Support</a>
						</li>
						<li class="nav-item download-button">
							<a class="nav-link menu-link btn" href="/download/setup.exe">Download</a>
						</li>
					</ul>
				</div>
			</nav>
		</div>
    </header>

    <div class="section section-pad section-bg nopb" id="overview">
	<div class="container">
        <div class="row justify-content-center text-center mb-100">
			<div class="col-lg-6 col-md-8">
				<div class="section-head-s8">
					<h1 class="section-title-s8 animated" data-animate="fadeInUp" data-delay=".1">Sign UP</h1>
				</div>
			</div>
		</div>
		<div class="row align-items-center mb-100">
			<div class="col-lg-6">
				<div class="graph-img res-m-btm animated" data-animate="fadeInUp"
				data-delay=".1"><img alt="graph" src="/assets/front/images/salvia/sample-840x500.png"></div>
			</div>
			<div class="col-lg-6">
                <div class="text-block">
                    <form class="support-form" action="">
                        <input type="email" name="" id="" placeholder="E-main">
                        <input type="text" name="" id="" placeholder="Login">
                        <input type="password" name="" id="" placeholder="Password">
                        <button type="submit">Submit</button>
                    </form>

                    <p><a href="signin">Already have a account ?</a></p>
				</div>
			</div>
        </div>

	</div>
</div>
<?php include 'footer.php';?>
<!--footer section end-->
        <!--Main js file load-->
        <script src="/assets/app/js/main.js"></script>
    </body>
    
<script src="/assets/front/js/jquery.bundle.js">
</script>
<script src="/assets/front/js/script.js">
</script>
</html>
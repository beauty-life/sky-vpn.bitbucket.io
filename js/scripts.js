"use strict";

function CheckScreenWidth($windowLength, $length) {
    return $windowLength <= $length;
}

function CarouselOnOff($window, $element) {
    if ($window.innerWidth <= 1100) {
        $element.attr("data-autoplay", "true");
    } else {
        $element.attr("data-autoplay", "false");
    }
}

function CheckCurrentPage($page) {
    var currentPage = $($page);

    currentPage.addClass("active");
}

function CheckLogo() {
    $("#logo-img").attr("src", "img/logoBigBlue.png");
}

function CreateHTMLAcccordion($id, $array) {
    for (var i = 0; i < $array.length; i++) {
        document.getElementById($id).innerHTML += "<li>" + $array[i] + "</li>";
    }
}

function SearchInArray($array, $element) {
    for (var i = 0; i < $array.length; i++) {
        if ($array[i].indexOf($element) != -1) {
            return true;
        }
    }

    return false;
}

function SearchElementInBlocklist(
    $ids,
    $element,
    $array1,
    $array2,
    $array3,
    $array4
) {
    var result = "";
    var element = "";

    if ($element.value == "") {
        if (document.getElementById($ids[0]).style.display == "block") {
            document.getElementById($ids[0]).style.display = "none";
        }
        return;
    }

    if ($element.value.indexOf("https://") != -1) {
        $element.value = $element.value.replace("https://", "");
    }

    if ($element.value.indexOf("http://") != -1) {
        $element.value = $element.value.replace("http://", "");
    }

    if ($element.value.indexOf("www.") != -1) {
        element = $element.value.replace("www.", "");
    } else {
        element = $element.value;
    }

    if (
        (SearchInArray($array1, element) ||
            SearchInArray($array2, element) ||
            SearchInArray($array3, element) ||
            SearchInArray($array4, element)) == true
    ) {
        if (document.getElementById($ids[0]).style.display == "none") {
            document.getElementById($ids[0]).style.display = "block";
        }

        document.getElementById($ids[1]).innerHTML = element;
        document.getElementById($ids[2]).innerHTML =
            '<h4><i class="fas fa-check text-success"></i></h4>';
        document.getElementById($ids[3]).innerHTML =
            "The requested site is in a blocklist";
    } else {
        if (document.getElementById($ids[0]).style.display == "none") {
            document.getElementById($ids[0]).style.display = "block";
        }

        document.getElementById($ids[1]).innerHTML = element;
        document.getElementById($ids[2]).innerHTML =
            '<h4><i class="fas fa-times text-danger"></i></h4>';
        document.getElementById($ids[3]).innerHTML =
            "The requested site was not found";
    }

    /*
    for(var i = 1; i < arguments.length; i++){
        for(var j = 0; j < arguments[i].length; j++){
            if(arguments[i][j].indexOf(element) != -1){
                document.getElementById('block2').innerText = arguments[i][j];
                //result = arguments[i][j];
            }
            else {
                document.getElementById('block2').innerText = arguments[i][j];
            }
        }
    }
*/

    /*
    result = element;

    
    
    alert(result);
    */
}

function HeaderCorrection($id, $className) {
    var header = document.getElementById($id);

    if (window.innerWidth <= 995) {
        if (header.classList.contains($className)) {
            header.classList.remove($className);
        }
    } else {
        if (!header.classList.contains($className)) {
            header.classList.add($className);
        }
    }
}

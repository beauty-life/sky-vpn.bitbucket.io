//onClick Display "Language" multilanguage box
function dropD(content) {
    var x = document.getElementById(content);
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}
//ScrollTop Button
function scrolltop(){
        // When the user scrolls down 20px from the top of the document, show the button
    window.onscroll = function() {scrollFunction()};

    function scrollFunction() {
        if (document.body.scrollTop > 320 || document.documentElement.scrollTop > 320) {
            document.getElementById("toTopButton").style.display = "block";
        } else {
            document.getElementById("toTopButton").style.display = "none";
        }
    } 
}
// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    function top() {
        window.scrollBy(0,-80); // чем меньше значение (цифра -10), тем выше скорость перемещения
            if (window.pageYOffset > 0) {requestAnimationFrame(top);} // если значение прокрутки больше нуля, то функция повториться
        }
    document.getElementById('toTopButton').addEventListener('click', function(e) {
        e.preventDefault();  // запрет перехода по ссылке, вместо него скрипт
        top();
    }, false);
}
//Tabs
function openFindTab(evt, cityName) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("findTabContent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("findTabLinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" activeTab", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " activeTab";
}
//DaysTabs
function openTT(evt, cityName) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("ttcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("ttlinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" activett", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " activett";
}
// + activeButton on current page
function current_page( page ){
    var cp = document.getElementById(page);
    cp.className += " activeButton";
}
// Get the element with id="defaultOpen" and click on it
function defaultTab(tab){
    document.getElementById(tab).click();
}
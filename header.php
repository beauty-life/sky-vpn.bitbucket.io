<?php include_once "functions.php";
?>
<!DOCTYPE html>
<html class="js">
<head>
    <!--META-->
	<meta charset="utf-8">
	<meta content="SKYVPN LLC" name="author">
	<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
	<meta name="description" content="SKYVPN Best VPN for any devices">
    <meta name="keywords" content="SKYVPN, best vpn, vpn service, fastest vpn, anonymus service, anonymus vpn, bypass browser lock, bypass site lock, hide ip, change ip,">
    <!--TITLE-->
    <title>SKYVPN</title>
    <!--LINKS-->
    <link href="/assets/images/logo/icon.png" rel="shortcut icon"> 
	<link href="/assets/front/css/vendor.bundle.css" rel="stylesheet">
	<link href="/css/style.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100italic,300,300italic,400,400italic,500,500italic,700,700italic,900,900italic" rel="stylesheet" type="text/css" />
        <!--Font Awasome-->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
    <link rel="stylesheet" href="../assets/front/css/FontAwasomeCSS/font-awesome.css">
    <!--SCRIPTS-->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <script type="text/javascript" src="/assets/user/js/functions.js"></script>
    <style>
        .d-button {
            min-width: 250px;
            border: 1px solid white;
            border-radius: 4px;
            background: transparent;
        }

        .d-button:hover {
            width: auto;
            box-shadow: none;
            padding: 25px 35px;
        }
    </style>
</head>